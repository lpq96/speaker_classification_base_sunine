#!/bin/bash

SPEAKER_TRAINER_ROOT=../../..
data_path=/work8/lipengqi/Projection/dataset/aishell-2

nnet_type=ResNet34_half_classification
pooling_type=TSP
loss_type=softmax
embedding_dim=512
scale=30.0
margin=0.1
cuda_device=0,1

stage=6


if [ $stage -le 1 ];then
  # prepare data
  if [ ! -d data/wav ]; then
    mkdir -p data/wav
  fi

  mkdir -p data/wav/train
  for spk in `ls ${data_path}`;do
    ln -s ${data_path}/${spk} data/wav/train

  done

  # for spk in `ls ${data_path}/test`;do
  #   ln -s ${data_path}/test/${spk} data/wav/train/$spk
  # done

  # for spk in `ls ${data_path}/dev`;do
  #   ln -s ${data_path}/dev/${spk} data/wav/train/$spk
  # done
  
fi

# <<COM
# if [ $stage -eq 2 ];then
#   # compute VAD for each dataset
#   echo Compute VAD $cnceleb1_root
#   python3 $SPEAKER_TRAINER_ROOT/steps/compute_vad.py \
#           --data_dir $CN_celeb1_path \
#           --extension wav \
#           --speaker_level 1 \
#           --num_jobs 8

#   # echo Compute VAD $cncceleb2_root
#   # python3 $SPEAKER_TRAINER_ROOT/steps/compute_vad.py \
#   #         --data_dir $$CN_celeb2_path/data \
#   #         --extension wav \
#   #         --speaker_level 1 \
#   #         --num_jobs 40
# fi

# <<COM
if [ $stage -le 3 ];then
  # prepare data for model training
  mkdir -p data
  echo Build train list
  python3 local/build_datalist_for_aishell.py \
          --data_dir data/wav/train \
          --extension wav \
          --speaker_level 1 \
          --data_list_path_train data/train_lst.csv \
          --data_list_path_test data/test_lst.csv

  # echo Build $musan_path list
  # python3 $SPEAKER_TRAINER_ROOT/steps/build_datalist.py \
  #         --data_dir $musan_path \
  #         --extension wav \
  #         --data_list_path data/musan_lst.csv

  # echo Build $rirs_path list
  # python3 $SPEAKER_TRAINER_ROOT/steps/build_datalist.py \
  #         --data_dir $rirs_path \
  #         --extension wav \
  #         --data_list_path data/rirs_lst.csv
fi

# <<COM
if [ $stage -le 4 ];then
  # model training
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --train_list_path data/train_lst.csv \
          --trials_path data/trials/CNC-Eval-Core.lst \
          --n_mels 40 \
          --max_frames 201 --min_frames 200 \
          --batch_size 200 \
          --nPerSpeaker 1 \
          --max_seg_per_spk 500 \
          --num_workers 8 \
          --max_epochs 31 \
          --loss_type $loss_type \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --learning_rate 0.01 \
          --lr_step_size 3 \
          --lr_gamma 0.50 \
          --margin $margin \
          --scale $scale \
          --eval_interval 5 \
          --eval_frames 0 \
          --save_top_k 20 \
          --distributed_backend dp \
          --reload_dataloaders_every_epoch \
          --gpus 2 \
          --suffix _128-mel
fi

# <<COM
if [ $stage -le 5 ];then
  # speaker classification evaluate
  # ckpt_path=exp/TDNN_TSP_512_softmax_30.0_0.1/epoch=23_train_loss=0.00.ckpt
  # ckpt_path=exp/TDNN_TSP_512_softmax_30.0_0.1_unsqueeze_tdnn/epoch=59_train_loss=0.00.ckpt
  # ckpt_path=exp/ResNet34_half_TSP_512_softmax_30.0_0.1/epoch=24_train_loss=0.00.ckpt
  ckpt_path=exp/ResNet34_half_TSP_512_softmax_30.0_0.1_128-mel/epoch=30_train_loss=0.00.ckpt
  result_list_path=result.csv
  echo Evaluate the speaker classification system
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --classification \
          --checkpoint_path $ckpt_path \
          --n_mels 128 \
          --trials_path data/trials/CNC-Eval-Core.lst \
          --train_list_path data/train_lst.csv \
          --test_list_path data/test_lst.csv \
          --result_list_path $result_list_path \
          --nnet_type $nnet_type \
          --loss_type $loss_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --scale $scale \
          --margin $margin \
          --num_workers 20 \
          --gpus 2
fi
if [ $stage -le 6 ];then
  # visualization through Score-CAM
  # ckpt_path=exp/TDNN_TSP_512_softmax_30.0_0.1/epoch=23_train_loss=0.00.ckpt
  # ckpt_path=exp/TDNN_TSP_512_softmax_30.0_0.1_unsqueeze_tdnn/epoch=59_train_loss=0.00.ckpt
  # ckpt_path=exp/ResNet34_half_TSP_512_softmax_30.0_0.1/epoch=24_train_loss=0.00.ckpt
  ckpt_path=exp/ResNet34_half_TSP_512_softmax_30.0_0.1_128-mel/epoch=30_train_loss=0.00.ckpt

  echo Visualization through Score-CAM
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --visualization \
          --checkpoint_path $ckpt_path \
          --n_mels 128 \
          --trials_path data/trials/CNC-Eval-Core.lst \
          --train_list_path data/train_lst.csv \
          --test_list_path data/test_lst.csv \
          --nnet_type $nnet_type \
          --loss_type $loss_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --scale $scale \
          --margin $margin \
          --num_workers 20 \
          --gpus 1
fi

