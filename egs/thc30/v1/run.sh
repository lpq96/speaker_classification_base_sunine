#!/bin/bash

SPEAKER_TRAINER_ROOT=../../..
thc30_path=/work8/lipengqi/Projection/dataset/thchs30/data_thchs30
thc30_test_path=/work8/lipengqi/Projection/dataset/thchs30/data_thchs30/test
data_dir=$PWD/data/wav
musan_path=/work102/lilt/database/musan
rirs_path=/work102/lilt/database/RIRS_NOISES

nnet_type=TDNN
pooling_type=TSP
loss_type=amsoftmax
embedding_dim=200
scale=30.0
margin=0.1
cuda_device=0

stage=4
#不需要
if [ $stage -le -1 ];then
  # flac to wav
  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $CN_celeb1_path/data \
          --speaker_level 1

  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $CN_celeb2_path/data \
          --speaker_level 1
 
  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $CN_celeb1_path/eval/enroll \
          --speaker_level 0
  
  python3 $SPEAKER_TRAINER_ROOT/steps/flac2wav.py \
          --dataset_dir $CN_celeb1_path/eval/test \
          --speaker_level 0
fi

if [ $stage -le 0 ];then
  # prepare data
  if [ ! -d data/wav ]; then
    mkdir -p $data_dir
  fi
  # 需要写一个数据准备，将thc30处理成 cnceleb的样子
  local/data_prepare.sh $thc30_path $data_dir
fi

if [ $stage -le 1 ];then
  mkdir -p data/trials
  python3 local/format_trials_thc30.py \
          --thc30_test_dir $data_dir/test \
          --dst_trl_path data/trials/thc30-Eval-Core.lst
fi
# <<COM
#需要做VAD
if [ $stage -eq 2 ];then
  # compute VAD for each dataset
  echo Compute VAD $thc30_path
  python3 $SPEAKER_TRAINER_ROOT/steps/compute_vad.py \
          --data_dir data/wav/train \
          --extension wav \
          --speaker_level 1 \
          --num_jobs 8
  # python3 $SPEAKER_TRAINER_ROOT/steps/compute_vad.py \
  #         --data_dir data/wav/test \
  #         --extension wav \
  #         --speaker_level 1 \
  #         --num_jobs 8
  
fi
# <<COM
if [ $stage -le 3 ];then
  # prepare data for model training
  mkdir -p data
  echo Build train list
  python3 $SPEAKER_TRAINER_ROOT/steps/build_datalist.py \
          --data_dir data/wav/train \
          --extension vad \
          --speaker_level 1 \
          --data_list_path data/train_lst.csv
  
  # # !不需要数据增强
  # echo Build $musan_path list
  # python3 $SPEAKER_TRAINER_ROOT/steps/build_datalist.py \
  #         --data_dir $musan_path \
  #         --extension wav \
  #         --data_list_path data/musan_lst.csv

  # echo Build $rirs_path list
  # python3 $SPEAKER_TRAINER_ROOT/steps/build_datalist.py \
  #         --data_dir $rirs_path \
  #         --extension wav \
  #         --data_list_path data/rirs_lst.csv
fi
# <<COM
if [ $stage -le 4 ];then
  # model training
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --train_list_path data/train_lst.csv \
          --trials_path data/trials/thc30-Eval-Core.lst \
          --n_mels 40 \
          --max_frames 201 --min_frames 200 \
          --batch_size 10 \
          --nPerSpeaker 1 \
          --max_seg_per_spk 500 \
          --num_workers 8 \
          --max_epochs 101 \
          --loss_type $loss_type \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --learning_rate 0.01 \
          --lr_step_size 3 \
          --lr_gamma 0.50 \
          --margin $margin \
          --scale $scale \
          --eval_interval 5 \
          --eval_frames 0 \
          --save_top_k 20 \
          --distributed_backend dp \
          --reload_dataloaders_every_epoch \
          --gpus 1
fi
<<COM
#这个暂时不需要
if [ $stage -le 5 ];then
  # evaluation
  ckpt_path=exp/*/*.ckpt

  echo Evaluate thc30-Eval-Core
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --checkpoint_path $ckpt_path \
          --n_mels 80 \
          --trials_path data/trials/thc30-Eval-Core.lst \
          --train_list_path data/train_list.csv \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --scale $scale \
          --margin $margin \
          --num_workers 20 \
          --gpus 1
fi

