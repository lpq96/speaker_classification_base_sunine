#!/bin/bash

train_thc30_path=$1/train
test_thc30_path=$1/test
dev_thc30_path=$1/dev

train_dest_dir=$2/train
test_dest_dir=$2/test
dev_dest_dir=$2/dev

traverse_dir()
{
    # filepath=$1
    for file in `ls $1`
    do
        if [ -d $1/$file ]
        then
            if [[ $file != '.' && $file != '..' ]]
            then
                traverse_dir $1/$file
            fi
        else
            check_suffix $1/$file $2
        fi
    done
}
 

check_suffix()
{
    file=$1
    
    if [ "${file##*.}"x = "wav"x ] || [ "${file##*.}"x = "vad"x ];then
        # echo $file
        wav_name=${file##*/}
        spk_id=${wav_name%_*}
        if [ -d $2/$spk_id ]
        then
          `ln -s $file $2/$spk_id/$wav_name`
        else
          mkdir -p $2/$spk_id
          `ln -s $file $2/$spk_id/$wav_name`
        fi
    fi    
}

traverse_dir $train_thc30_path $train_dest_dir
traverse_dir $test_thc30_path $test_dest_dir
traverse_dir $dev_thc30_path $dev_dest_dir

