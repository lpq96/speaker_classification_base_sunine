#!/usr/bin/env python
# encoding: utf-8

import os
import argparse
import sys
import numpy as np
from pydub import AudioSegment  # 先导入这个模块

def combination_audio(test_dir):
    #读 test_dir里的文件
    # 创建 spk2utt
    spk2utt = {}
    for spk_id in os.listdir(test_dir):
      spk_path = os.path.join(test_dir,spk_id)
      if os.path.isdir(spk_path):
        spk2utt[spk_id] = []
        for wav_name in os.listdir(spk_path):
            spk2utt[spk_id].append(wav_name)
    # 根据 spk2utt 将每一个说话人的前5句话 combination到一起
    # 加载需要合并的两个mp3音频
    for spk, utt in spk2utt.items():
        input_audio_0 = AudioSegment.from_file(os.path.join(test_dir, spk, utt[0]),format='wav')
        input_audio_1 = AudioSegment.from_file(os.path.join(test_dir, spk, utt[1]),format='wav')
        input_audio_2 = AudioSegment.from_file(os.path.join(test_dir, spk, utt[2]),format='wav')
        input_audio_3 = AudioSegment.from_file(os.path.join(test_dir, spk, utt[3]),format='wav')
        input_audio_4 = AudioSegment.from_file(os.path.join(test_dir, spk, utt[4]),format='wav')
        output_music = input_audio_0 +input_audio_1+input_audio_2+input_audio_3+input_audio_4
        output_path = os.path.join(test_dir, spk+'-enroll.wav')
        output_music.export(output_path, format="wav")# 前面是保存路径，后面是保存格式
    return spk2utt


def make_trails(test_dir , spk2utt, trials, apply_vad):
     # read utt2spk
    spk_set, utt_set = set(), set()
    for spk, utt in spk2utt.items():
        spk_enroll_wav_path = os.path.join(test_dir,spk+'-enroll.wav')
        spk_set.add(spk_enroll_wav_path)
        for i in range(len(utt)):
          if i > 5:
            utt_wav_path = os.path.join(test_dir,spk,utt[i])
            utt_set.add(utt_wav_path)
    with open(trials, 'w') as ftrial:
        for utt_path in utt_set:
            spk_id = os.path.basename(utt_path).split('_')[0]
            if apply_vad:
                  utt_path = utt_path.strip("*.wav") + ".vad"
            for spk_enroll_path in spk_set:
                if apply_vad:
                  spk_enroll_path = spk_enroll_path.strip("*.wav") + ".vad"
                if os.path.basename(spk_enroll_path).split('-')[0] == spk_id:
                    ftrial.write('{} {} {}\n'.format('1',spk_enroll_path, utt_path ))
                else:
                    ftrial.write('{} {} {}\n'.format('0',spk_enroll_path, utt_path))


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--thc30_test_dir', help='thc30 test dir', type=str, default="$data_dir/test")
    parser.add_argument('--dst_trl_path', help='output trial path', type=str, default="new.trials")
    parser.add_argument('--apply_vad', action='store_true', default=False)
    args = parser.parse_args()

    # combination_audio
    spk2utt = combination_audio(args.thc30_test_dir)
    make_trails(args.thc30_test_dir , spk2utt, args.dst_trl_path, args.apply_vad)
    # enroll_lst_path = os.path.join(args.cnceleb_root, "eval/lists/enroll.lst")
    # raw_trl_path = os.path.join(args.cnceleb_root, "eval/lists/trials.lst")

    # spk2wav_mapping = {}
    # enroll_lst = np.loadtxt(enroll_lst_path, str)
    # for item in enroll_lst:
    #     spk2wav_mapping[item[0]] = item[1]  # spk2wav_mapping[id00835-enroll]=enroll/id00835-enroll.wav
    # trials = np.loadtxt(raw_trl_path, str)

    # with open(args.dst_trl_path, "w") as f:
    #     for item in trials: # id00800-enroll test/id00801-entertainment-01-002.wav 0
    #         enroll_path = os.path.join(args.cnceleb_root, "eval", spk2wav_mapping[item[0]])
    #         test_path = os.path.join(args.cnceleb_root, "eval", item[1])
    #         if args.apply_vad:
    #             enroll_path = enroll_path.strip("*.wav") + ".vad"
    #             test_path = test_path.strip("*.wav") + ".vad"
    #         label = item[2]
    #         f.write("{} {} {}\n".format(label, enroll_path, test_path))

