#!/bin/bash

SPEAKER_TRAINER_ROOT=../../..
voxceleb1_root=/ssd/lilt/database/VoxCeleb/voxceleb1/voxceleb1_wav
voxceleb2_root=/ssd/lilt/database/VoxCeleb/voxceleb2/dev/aac
sitw_dev_root=/ssd/lilt/database/SITW/dev
sitw_eval_root=/ssd/lilt/database/SITW/eval
# musan_path=/ssd/lilt/database/musan
# rirs_path=/ssd/lilt/database/RIRS_NOISES

nnet_type=ResNet34_half
pooling_type=TSP
loss_type=aamsoftmax
embedding_dim=256
scale=30.0
margin=0.2
cuda_device=1

stage=6

if [ $stage -le 0 ];then
  # evaluation
  ckpt_path=exp/${nnet_type}_${pooling_type}_${embedding_dim}_${loss_type}_${margin}/epoch=44_train_loss=0.77.ckpt

  echo Evaluate SITW-Dev-Core
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --checkpoint_path $ckpt_path \
          --n_mels 80 \
          --trials_path data/trials/SITW-Dev-Core.lst \
          --scores_path SITW-Dev-Core.foo \
          --train_list_path data/train_list.csv \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --num_workers 20 \
          --gpus 1

  echo Evaluate SITW-Core-Easy
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --checkpoint_path $ckpt_path \
          --n_mels 80 \
          --trials_path data/trials/SITW-Core-Easy.lst \
          --scores_path SITW-Core-Easy.foo \
          --train_list_path data/train_list.csv \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --margin $margin \
          --num_workers 20 \
          --gpus 1

  echo Evaluate SITW-Core-Norm
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --checkpoint_path $ckpt_path \
          --n_mels 80 \
          --trials_path data/trials/SITW-Core-Norm.lst \
          --scores_path SITW-Core-Norm.foo \
          --train_list_path data/train_list.csv \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --margin $margin \
          --num_workers 20 \
          --gpus 1


  echo Evaluate SITW-Core-Hard
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --checkpoint_path $ckpt_path \
          --n_mels 80 \
          --trials_path data/trials/SITW-Core-Hard.lst \
          --scores_path SITW-Core-Hard.foo \
          --train_list_path data/train_list.csv \
          --nnet_type $nnet_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --margin $margin \
          --num_workers 20 \
          --gpus 1
fi

if [ $stage -le 5 ];then
  # speaker classification evaluate
  # ckpt_path=exp/TDNN_TSP_512_softmax_30.0_0.1/epoch=23_train_loss=0.00.ckpt
  # ckpt_path=exp/TDNN_TSP_512_softmax_30.0_0.1_unsqueeze_tdnn/epoch=59_train_loss=0.00.ckpt
  # ckpt_path=exp/ResNet34_half_TSP_512_softmax_30.0_0.1/epoch=24_train_loss=0.00.ckpt
  ckpt_path=exp/ResNet34_half_TSP_256_aamsoftmax_0.2/epoch=24_train_loss=0.78.ckpt
  result_list_path=result.csv
  echo Evaluate the speaker classification system
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --checkpoint_path $ckpt_path \
          --keep_loss_weight \
          --n_mels 80 \
          --trials_path data/trials/CNC-Eval-Core.lst \
          --train_list_path data/train_lst.csv \
          --test_list_path data/train_lst_some.csv \
          --result_list_path $result_list_path \
          --nnet_type $nnet_type \
          --loss_type $loss_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --scale $scale \
          --margin $margin \
          --num_workers 20 \
          --gpus 1
fi
if [ $stage -le 6 ];then
  # visualization through Score-CAM
  # ckpt_path=exp/TDNN_TSP_512_softmax_30.0_0.1/epoch=23_train_loss=0.00.ckpt
  # ckpt_path=exp/TDNN_TSP_512_softmax_30.0_0.1_unsqueeze_tdnn/epoch=59_train_loss=0.00.ckpt
  # ckpt_path=exp/ResNet34_half_TSP_512_softmax_30.0_0.1/epoch=24_train_loss=0.00.ckpt
  # ckpt_path=exp/ResNet34_half_TSP_512_softmax_30.0_0.1_128-mel/epoch=30_train_loss=0.00.ckpt
  ckpt_path=exp/ResNet34_half_TSP_256_aamsoftmax_0.2/epoch=24_train_loss=0.78.ckpt

  echo Visualization through Score-CAM
  CUDA_VISIBLE_DEVICES=$cuda_device python3 -W ignore $SPEAKER_TRAINER_ROOT/main.py \
          --evaluate \
          --visualization \
          --keep_loss_weight \
          --checkpoint_path $ckpt_path \
          --n_mels 80 \
          --trials_path data/trials/CNC-Eval-Core.lst \
          --train_list_path data/train_lst.csv \
          --test_list_path data/train_lst_some.csv \
          --nnet_type $nnet_type \
          --loss_type $loss_type \
          --pooling_type $pooling_type \
          --embedding_dim $embedding_dim \
          --scale $scale \
          --margin $margin \
          --num_workers 20 \
          --gpus 1
fi

