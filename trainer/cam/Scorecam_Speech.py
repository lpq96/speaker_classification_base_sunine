from logging import log
import torch
import torch.nn.functional as F
from .basecam import *
import sys
import matplotlib.pyplot as plt


class ScoreCAM_Speech(BaseCAM):

    """
        ScoreCAM, inherit from BaseCAM
        ScoreCAM for speech domain 
        input: log-mel specturm
        model: the last layer of Resnet network 
    """

    def __init__(self, model_dict):
        super().__init__(model_dict)

    def forward(self, input, class_idx=None, retain_graph=False):
        c, h, w = input.size()  # b:1,c:3,h:224,w:224
        
        # predication on raw input
        logit = self.model_arch(input).cuda()
        if class_idx is None:
            predicted_class = logit.max(1)[-1]
            score = logit[:, logit.max(1)[-1]].squeeze().requires_grad_()

        else:
            predicted_class = torch.LongTensor([class_idx])
            score = logit[:, class_idx].squeeze()
        
        logit = F.softmax(logit,1)

        if torch.cuda.is_available():
          predicted_class = predicted_class.cuda()
          score = score.cuda()
          logit = logit.cuda()

        self.model_arch.zero_grad()
        score.backward(retain_graph=retain_graph)
        activations = self.activations['value']
        b, k, u, v = activations.size()
        score_saliency_map = torch.zeros((1, 1, h, w))
        if torch.cuda.is_available():
          activations = activations.cuda()
          score_saliency_map = score_saliency_map.cuda()

          
        with torch.no_grad():
          for i in range(k):

              # upsampling
              saliency_map = torch.unsqueeze(activations[:, i, :, :], 1)
              
              saliency_map = F.interpolate(saliency_map, size=(h, w), mode='bilinear', align_corners=False)
              # todo analysis some channel feature map
              # if i % 10 == 0:
              #     feature_map = (saliency_map).cpu().numpy()[0,0, :, :]
              #     plt.figure()
              #     sns.heatmap(feature_map,cmap='rainbow')
              #     plt.savefig('./maps/the_one_scorecam_map{}.png'.format(i))
              #     plt.close()
              if saliency_map.max() == saliency_map.min():
                continue

              # normalize to 0-1
              norm_saliency_map = (saliency_map - saliency_map.min()) / (saliency_map.max() - saliency_map.min())

              # how much increase if keeping the highlighted region
              # predication on masked input
              norm_saliency_map = torch.squeeze(norm_saliency_map,1)
              output = self.model_arch(input * norm_saliency_map)
             
              output = F.softmax(output,1)
              score = output[0][predicted_class]

              score_saliency_map +=  score * saliency_map
             
        score_saliency_map = F.relu(score_saliency_map)
        score_saliency_map_min, score_saliency_map_max = score_saliency_map.min(), score_saliency_map.max()

        if score_saliency_map_min == score_saliency_map_max:
            return None

        score_saliency_map = (score_saliency_map - score_saliency_map_min).div(score_saliency_map_max - score_saliency_map_min).data

        return score_saliency_map

    def __call__(self, input, class_idx=None, retain_graph=False):
        return self.forward(input, class_idx, retain_graph)