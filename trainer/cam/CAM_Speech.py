from logging import log
import torch
import torch.nn.functional as F
from .basecam import *
import sys
import matplotlib.pyplot as plt


class CAM_Speech(BaseCAM):

    """
        CAM, inherit from BaseCAM
        the average of the last layer conv feature maps
        Ref: https://ietresearch.onlinelibrary.wiley.com/doi/epdf/10.1049/ell2.12354
    """

    def __init__(self, model_dict):
        super().__init__(model_dict)

    def forward(self, input, class_idx=None, retain_graph=False):
        c, h, w = input.size()  # b:1,c:3,h:224,w:224
        
        # # predication on raw input
        logit = self.model_arch(input).cuda()
        
        self.model_arch.zero_grad()
        activations = self.activations['value']
        b, k, u, v = activations.size()

        cam_saliency_map = torch.zeros((1, 1, h, w))
        if torch.cuda.is_available():
          cam_saliency_map = cam_saliency_map.cuda()
          
        with torch.no_grad():
          for i in range(k):

              # upsampling
              saliency_map = torch.unsqueeze(activations[:, i, :, :], 1)
              saliency_map = F.interpolate(saliency_map, size=(h, w), mode='bilinear', align_corners=False)
              if saliency_map.max() == saliency_map.min():
                continue
              cam_saliency_map += saliency_map
              
        final_saliency_map = cam_saliency_map.div(k) 
        final_saliency_map = F.relu(final_saliency_map)
        final_saliency_map_min, final_saliency_map_max = final_saliency_map.min(), final_saliency_map.max()

        if final_saliency_map_min == final_saliency_map_max:
            return None

        final_saliency_map = (final_saliency_map - final_saliency_map_min).div(final_saliency_map_max - final_saliency_map_min).data

        return final_saliency_map

    def __call__(self, input, class_idx=None, retain_graph=False):
        return self.forward(input, class_idx, retain_graph)